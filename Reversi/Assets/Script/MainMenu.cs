﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Button NewGameButton;

    public Button SettingsButton;

    public Button ExitGameButton;

   

    public GameObject Settings;
    public void NewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    
    public void OpenSettingsMenu()
    {
        Settings.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
